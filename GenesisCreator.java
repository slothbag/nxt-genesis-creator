/***************************************************************************************************
How to use:
* Copy this java file into your nxt/src/java/nxt folder
* Update the addTx section to fund which ever addresses you wish
* run ./compile.sh
* run java -cp nxt.jar nxt.GenesisCreator
  This will output all the variables required for a new genesis block
* Update the Genesis.java that came with the NRS with the variables outputted above
* run ./compile.sh again

Notes
=====
Although this will generate a valid GenesisBlock, NXT will not be able to generate a valid blockchain
unless the checkpoints and transparent forging start points are removed.  Thats what the patch file 
will do. Patch NXT and compile to allow a clean blockchain to start start building.

***************************************************************************************************/
package nxt;

import nxt.util.Logger;
import nxt.crypto.Crypto;
import nxt.util.Convert;

import java.security.MessageDigest;
import java.util.SortedMap;
import java.util.TreeMap;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

class GenesisCreator {

	public static final String secretPhrase = "It was a bright cold day in April, and the clocks were striking thirteen.";
	public static SortedMap<Long,TransactionImpl> transactionsMap = new TreeMap<>();
	public static byte[] pubkey;
		
    public static void main(String[] args) {
		try {			
			pubkey = Crypto.getPublicKey(secretPhrase);
			System.out.println("PubKey: " + Convert.toHexString(pubkey));
			
			addTx("17426429710965382789",1000000000);
			/*addTx("163918645372308887",36742);
			addTx("620741658595224146",1970092);
			addTx("723492359641172834",349130);
			addTx("818877006463198736",24880020);
			addTx("1264744488939798088",2867856);
			addTx("1600633904360147460",9975150);
			addTx("1796652256451468602",2690963);
			addTx("1814189588814307776",7648);
			addTx("1965151371996418680",5486333);
			addTx("2175830371415049383",34913026);
			addTx("2401730748874927467",997515);
			addTx("2584657662098653454",30922966);
			addTx("2694765945307858403",6650);
			addTx("3143507805486077020",44888);
			addTx("3684449848581573439",2468850);
			addTx("4071545868996394636",49875751);
			addTx("4277298711855908797",49875751);
			addTx("4454381633636789149",9476393);
			addTx("4747512364439223888",49875751);
			addTx("4777958973882919649",14887912);
			addTx("4803826772380379922",528683);
			addTx("5095742298090230979",583546);
			addTx("5271441507933314159",7315);
			addTx("5430757907205901788",19925363);
			addTx("5491587494620055787",29856290);
			addTx("5622658764175897611",5320);
			addTx("5982846390354787993",4987575);
			addTx("6290807999797358345",5985);
			addTx("6785084810899231190",24912938);
			addTx("6878906112724074600",49875751);
			addTx("7017504655955743955",2724712);
			addTx("7085298282228890923",1482474);
			addTx("7446331133773682477",200999);
			addTx("7542917420413518667",1476156);
			addTx("7549995577397145669",498758);
			addTx("7577840883495855927",987540);
			addTx("7579216551136708118",16625250);
			addTx("8278234497743900807",5264386);
			addTx("8517842408878875334",15487585);
			addTx("8870453786186409991",2684479);
			addTx("9037328626462718729",14962725);
			addTx("9161949457233564608",34913026);
			addTx("9230759115816986914",5033128);
			addTx("9306550122583806885",2916900);
			addTx("9433259657262176905",49875751);
			addTx("9988839211066715803",4962637);
			addTx("10105875265190846103",170486123);
			addTx("10339765764359265796",8644631);
			addTx("10738613957974090819",22166945);
			addTx("10890046632913063215",6668388);
			addTx("11494237785755831723",233751);
			addTx("11541844302056663007",4987575);
			addTx("11706312660844961581",11083556);
			addTx("12101431510634235443",1845403);
			addTx("12186190861869148835",49876);
			addTx("12558748907112364526",3491);
			addTx("13138516747685979557",3491);
			addTx("13330279748251018740",9476);
			addTx("14274119416917666908",49876);
			addTx("14557384677985343260",6151);
			addTx("14748294830376619968",682633);
			addTx("14839596582718854826",49875751);
			addTx("15190676494543480574",482964);
			addTx("15253761794338766759",4988);
			addTx("15558257163011348529",49875751);
			addTx("15874940801139996458",4988);
			addTx("16516270647696160902",9144);
			addTx("17156841960446798306",503745);
			addTx("17228894143802851995",49875751);
			addTx("17240396975291969151",52370);
			addTx("17491178046969559641",29437998);
			addTx("18345202375028346230",585375);
			addTx("18388669820699395594",9975150);*/

			MessageDigest digest = Crypto.sha256();
			int total_amount = 0;
            for (Transaction tx : transactionsMap.values()) {
                digest.update(tx.getBytes());
				total_amount += tx.getAmount();
            }
			
			System.out.println("Total Amount: " + total_amount);
			
			byte[] payloadHash = digest.digest();
			
			//calculate block signature
			byte[] block_signature = new byte[64];
			
			ByteBuffer block_buffer = ByteBuffer.allocate(4 + 4 + 8 + 4 + 4 + 4 + 4 + 32 + 32 + (32 + 32) + 64);
			block_buffer.order(ByteOrder.LITTLE_ENDIAN);
			block_buffer.putInt(-1); //version
			block_buffer.putInt(0); //timestamp
			block_buffer.putLong(0); //previous block id
			block_buffer.putInt(transactionsMap.size()); //transaction size
			block_buffer.putInt(total_amount); //totalAmount
			block_buffer.putInt(0); //totalFee
			block_buffer.putInt(transactionsMap.size() * 128); //payloadLength
			block_buffer.put(payloadHash); //payloadHash
			block_buffer.put(pubkey); //generatorPublicKey
			block_buffer.put(new byte[64]); //generationSignature
			block_buffer.put(block_signature); //blocksignature
		
			byte[] data = block_buffer.array();       
			byte[] data2 = new byte[data.length - 64];
			System.arraycopy(data, 0, data2, 0, data2.length);
			block_signature = Crypto.sign(data2, secretPhrase);
			
			System.out.print("Block Signature: ");
			for (int a=0;a<block_signature.length;a++)
				System.out.print(block_signature[a] + ", ");
			System.out.println("");

            BlockImpl genesisBlock = new BlockImpl(-1, 0, null, total_amount, 0, transactionsMap.size() * 128, payloadHash,
                    pubkey, new byte[64], block_signature, null, new ArrayList<>(transactionsMap.values()));

            genesisBlock.setPrevious(null);
			System.out.println("Block ID: " + genesisBlock.getId());
			
			System.out.println("Done");
			
		} catch (NxtException.ValidationException e) {
            throw new RuntimeException(e.toString(), e);
        }
    }
	
	public static TransactionImpl addTx(String acct, int amount) {
	
			Long rx_acct = (new BigInteger(acct)).longValue();
	
			byte zero_byte = (byte)0;
			byte[] signature = new byte[64];
			ByteBuffer buffer = ByteBuffer.allocate(128);
			buffer.order(ByteOrder.LITTLE_ENDIAN);
			buffer.put(zero_byte);
			buffer.put(zero_byte);
			buffer.putInt(zero_byte);
			buffer.putShort(zero_byte);
			buffer.put(pubkey);
			buffer.putLong(Convert.nullToZero(rx_acct));
			buffer.putInt(amount);
			buffer.putInt(zero_byte);
			buffer.putLong(zero_byte);
			buffer.put(signature);
			
			signature = Crypto.sign(buffer.array(), secretPhrase);
	
			System.out.print("TX Signature: ");
			for (int a=0;a<signature.length;a++)
				System.out.print(signature[a] + ", ");
			System.out.println("");
				
			TransactionImpl transaction = null;
			try {
				//create the genesis txfr
				transaction = new TransactionImpl(TransactionType.Payment.ORDINARY, 0, (short) 0, pubkey,
						rx_acct, amount, 0, null, signature);			
			}
			catch (Exception ex) {}
			
			transactionsMap.put(transaction.getId(), transaction);
			
			return transaction;
	}
}
